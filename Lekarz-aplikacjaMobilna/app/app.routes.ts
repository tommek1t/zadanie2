import {RouterConfig} from "@angular/router";
import {nsProvideRouter} from "nativescript-angular/router";
import {MainViewComponent} from './main-view.component';
import {VisitDetailComponent} from './visit-detail.component'
import {LoginComponent} from './login.component';
import {VisitReadComponent} from './visit-read-only.component';


export const routes: RouterConfig = [
  { path: "", component: MainViewComponent },
  {path: 'visit-detail', component: VisitDetailComponent},
  {path: 'visit-detail/:id', component: VisitDetailComponent},
  {path: 'login', component: LoginComponent},
  {path: 'visit-readOnly/:id/:reservation', component: VisitReadComponent},
  {path: 'visit-readOnly/:id', component: VisitReadComponent}
]


export const APP_ROUTER_PROVIDERS = [
  nsProvideRouter(routes, {})
];