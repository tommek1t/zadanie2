import "reflect-metadata";
import {HTTP_PROVIDERS} from "@angular/http";
import {nativeScriptBootstrap} from "nativescript-angular/application";
import {AppComponent} from "./app.component";
import {APP_ROUTER_PROVIDERS} from "./app.routes";
import {UserService} from './user.service';

nativeScriptBootstrap(AppComponent, [UserService, HTTP_PROVIDERS, APP_ROUTER_PROVIDERS]);