import {Component, OnInit} from "@angular/core";
import {Router} from '@angular/router';
import {UserService} from "../user.service";
import {alert} from "ui/dialogs";

@Component({
    selector: "spec-okres",
    templateUrl: "html/spec-okres.component.html",
    styleUrls: ["css/forms.css"]
})
export class SpecOkresComponent implements OnInit {
    lista: string[];
    dataOD: Date;
    dataDO: Date;
    specjalista: number;
    chceSpecjalizacje: boolean = true;
    chceDateDO: boolean = true;

    public selectedIndexChanged(picker) {
        this.specjalista = picker.selectedIndex;
    }

    next() {
        if (this.chceSpecjalizacje === false)
            this.specjalista = -1;

        if (this.chceDateDO === false)
            this.dataDO = new Date(2050, 12, 31, 23, 59);

        if (this.dataDO < this.dataOD)
            alert("Data końcowa jest wcześniejsza niż poczatkowa!");
        else {
            Promise.resolve().then(()=>this.userService.setOkres(this.dataOD, this.dataDO))
            .then(()=> this.userService.setSpec(this.specjalista))
            .then(()=>this.router.navigate(["/cities"]));
        }
    }

    jumpIt(elem: string) {
        if (elem == 's') {
            this.chceSpecjalizacje = !this.chceSpecjalizacje;
        }

        if (elem == 'd2') {
            this.chceDateDO = !this.chceDateDO;
        }
    }

    constructor(private router: Router, private userService: UserService) {
    }

    ngOnInit() {
        this.userService.getActualSpecialization().then(spec=>this.lista=spec);
    }
}
