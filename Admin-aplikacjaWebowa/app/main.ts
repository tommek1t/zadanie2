import {DoctorService} from './doctor.service';
// The usual bootstrapping imports
import { bootstrap }      from '@angular/platform-browser-dynamic';
import { HTTP_PROVIDERS } from '@angular/http';

import { AppComponent }         from './app.component';
import { APP_ROUTER_PROVIDERS } from './app.routes';

bootstrap(AppComponent, [DoctorService, HTTP_PROVIDERS, APP_ROUTER_PROVIDERS]);