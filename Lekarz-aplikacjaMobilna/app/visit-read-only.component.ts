import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from '@angular/router';
import {DoctorService} from './doctor.service';
import timePicker = require("ui/time-picker");

@Component({
    selector: "visit",
    templateUrl: "visit-read-only.component.html"
})

export class VisitReadComponent implements OnInit {
    sub: any;
    visit: any;
    reservation: any;

    constructor(private doctorService: DoctorService, private route: ActivatedRoute, private router: Router) { }


    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            let id = params['id'];
            this.doctorService.getVisit(id)
                .then(v => this.visit = v);
            let res = params['reservation'];
            if (res !== undefined)
                this.doctorService.getReservationByID(res).then(data=>this.reservation=data);
        })
    }

    back() {
        this.router.navigate(['/']);
    }
}