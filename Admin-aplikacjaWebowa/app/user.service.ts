import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {User} from './user';


@Injectable()
export class UserService {

    private userUrl = 'app/users';
    constructor(private http: Http) { }
    
    getAllUsers(): Promise<User[]> {
        return this.http.get(this.userUrl)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    getUser(login: string) {
        return this.getAllUsers().then(users => users.find(user => user.login === login));
    }

    checkLog(login: string, password: string) {
        return this.getUser(login).then(user => (user.passwd == password));
    }

    getUserByID(id: number) {
        return this.getAllUsers().then(users => users.find(user => user.id === id));
    }

    getUserName(login: string) {
        return this.getUser(login).then(user => (user.name));
    }

    getUserID(login: string) {
        return this.getUser(login).then(user => (user.id));
    }

    private handleError(error: any) {
        console.error('Wystapil blad!! (userService)', error);
        return Promise.reject(error.message || error);
    }
}