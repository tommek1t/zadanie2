import {Component,OnInit} from "@angular/core";
import {Router} from '@angular/router';
import {UserService} from './user.service';
var applicationSettings = require("application-settings");

@Component({
    selector: "menu",
    templateUrl: "html/menu.component.html",
    styleUrls: ["css/forms.css"]
})
export class MenuComponent implements OnInit{
    name:string="";

    jumpIt(elem: string) {
        if (elem == 'new') {
            this.router.navigate(['/spec-okres']);
        }

        if (elem == 'view') {
            this.router.navigate(['/myreservation']);
        }

        if (elem == 'logout') {
            this.userService.user=undefined;
            applicationSettings.setString("id_user", "none");          
            this.router.navigate(['/']);
        }
    }

    constructor(private router: Router, private userService:UserService) {}

    ngOnInit(){
        this.userService.getUserName().then(userName=>this.name=userName);
    }


}
