import {Component } from '@angular/core';
import { ROUTER_DIRECTIVES} from '@angular/router';
import {DoctorService} from './doctor.service';
import {LoginComponent} from './login.component'
import {UserService} from './user.service'

@Component({
    selector: 'my-app',
    template: `<h1>{{title}}</h1>
    <login-comp></login-comp>
    <nav>
        <a [routerLink]="['/doctors']">Lekarze</a>
    </nav>
    
    <router-outlet></router-outlet>`,
    styleUrls:['app/css/app.component.css'],
    directives: [ROUTER_DIRECTIVES,LoginComponent],
    providers: [DoctorService,UserService]

})

export class AppComponent {
    title = "Administracja serwisu";
}