import {Component, OnInit} from "@angular/core";
import {Router} from '@angular/router'
import {UserService} from "../user.service";

@Component({
    selector: "visit",
    templateUrl: "html/visit.component.html",
    styleUrls: ["css/forms.css"]
})
export class VisitComponent implements OnInit {
    lista: string[] = [];

    public listViewItemTap(pozycja) {
        Promise.resolve().then(()=>this.userService.setChooseVisit(pozycja.index))
        .then(()=>this.router.navigate(["/summary"]));
    }

    public back() {
        this.userService.cities = [];
        this.router.navigate(["/spec-okres"]);
    }

    constructor(private router: Router, private userService: UserService) {
    }

    ngOnInit() {
        this.userService.getSpecyficVisits().then(obiekty => {
            obiekty.forEach(el => {
                console.log(el)
                this.lista.push(el.date+", od " + el.hour1 + " do " + el.hour2 + "\n ("
                    + el.city + ", " + el.doctor_specjalizacja + ")");
            });
        });
    }
}
