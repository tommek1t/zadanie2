import {Component, OnInit} from "@angular/core";
import {Router} from '@angular/router'
var applicationSettings = require("application-settings");
import {DoctorService} from './doctor.service';

@Component({
    selector: "login",
    templateUrl: "login.component.html",
    styleUrls: ["app.css"]
})
export class LoginComponent implements OnInit {
    id: string = "";
    password: string = "";

    constructor(private router: Router, private doctorService: DoctorService) {
    }

    tryLogIn() {
        if (this.id.length > 20 && this.password.length > 0) {
            this.doctorService.tryLog(this.id, this.password).subscribe(
                () => {
                    applicationSettings.setString("id", this.id);
                    applicationSettings.setString("password", this.id);
                    this.doctorService.setDoctor(this.id).then(() => this.router.navigate(['/']));
                },
                (error) => alert("Niestety nie możemy znaleźć twojego konta. Sprawdź podane dane.")
            );
        }
        else
            alert("Sprawdź dane logowania i spróbuj ponownie.");

    }

    ngOnInit() {
        if (!applicationSettings.hasKey("id")){
            this.id = applicationSettings.getString("id");
            this.password=applicationSettings.getString("password");
        }
    }
}
