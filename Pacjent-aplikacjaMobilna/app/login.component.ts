import {Component, OnInit} from "@angular/core";
import {Router} from '@angular/router'
import {UserService} from "./user.service";
var applicationSettings = require("application-settings");

@Component({
    selector: "login",
    templateUrl: "html/login.component.html",
    styleUrls: ["css/forms.css"]
})
export class LoginComponent implements OnInit {
    id: number;
    password: string;


    public tryLogIn() {
        if (this.id.toString().length === 0 || this.password.length === 0)
            alert("Nie podałeś wszystkich danych uwierzytelniajacych!");
        else
            this.userService.tryLog(this.id, this.password).subscribe(
                () => {
                    applicationSettings.setString("id_user", this.id);
                    Promise.resolve(this.userService.setUser(this.id))
                    .then(()=>alert("Zalogowano poprawnie"))
                        .then(() => this.router.navigate(["/myreservation"]))
                },
                (error) => alert("Niestety nie możemy znaleźć twojego konta. Sprawdź podane dane.")
            );
    }

    constructor(private router: Router, private userService: UserService) {
    }

    ngOnInit() {
        let iduser = applicationSettings.getString("id_user", "none");
        if (iduser !== "none") {
            this.id = iduser;
            this.tryLogIn();
        }
    }
}
