import {RouterConfig} from "@angular/router";
import {nsProvideRouter} from "nativescript-angular/router"
import {LoginComponent} from "./login.component";
import {SpecOkresComponent} from './setReservationComponents/spec-okres.component';
import {CitiesComponent} from './setReservationComponents/cities.component';
import {VisitComponent} from './setReservationComponents/visit.component';
import {SummaryComponent} from './setReservationComponents/summary.component'
import {MyReservationComponent} from './myreservation.component'

export const routes: RouterConfig = [
  { path: "", component: LoginComponent },
  {path: "spec-okres", component: SpecOkresComponent},
  {path: "cities", component: CitiesComponent},
  {path: "visits", component: VisitComponent},
  {path: "summary", component:SummaryComponent},
  {path: "myreservation", component: MyReservationComponent},
  {path: "reservation-details", component: SpecOkresComponent},
  {path: "delreservation", component: SpecOkresComponent}
];

export const APP_ROUTER_PROVIDERS = [
  nsProvideRouter(routes, {})
];