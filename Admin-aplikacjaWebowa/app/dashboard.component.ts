import { Component, OnInit } from '@angular/core';
import {Doctor} from './doctor';
import {DoctorService} from './doctor.service';
import {Router} from '@angular/router';

@Component({
  selector: 'my-dashboard',
  templateUrl: 'app/html/dashboard.component.html',
  styleUrls: ['app/css/dashboard.component.css']
})
export class DashboardComponent implements OnInit{
    ngOnInit(){
        this.doctorService.getDoctors().then(heroes=>this.doctors=heroes.slice(2,6));
    }
    doctors: Doctor[]=[];
    constructor(private doctorService: DoctorService,private router:Router){}

    gotoDetail(doc:Doctor){
        let link=['/detail',doc._id];
        this.router.navigate(link);
    }
 }