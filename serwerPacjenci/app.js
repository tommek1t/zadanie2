var express = require('express');
var bodyParser = require('body-parser');
var _ = require('lodash');
var app = express();
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var async = require('async');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:3009");
  res.header("Access-Control-Allow-Headers", "Origin");
  next();
});

var urlDB = 'mongodb://localhost:27017/zad2';

var db;
MongoClient.connect(urlDB, function (err, database) {
  if (err)
    process.exit(1);

  db = database;
  app.listen(3009);
  console.log("Listening on port 3009");
});



app.get("/app/users", function (req, res, next) {
  db.collection('users').find().toArray(function (err, user) {
    if (err)
      return next(err);
    res.send(user);
  });
});


app.get("/app/visits", function (req, res, next) {
  db.collection('reservation').find({}, { "Visit._id": 1 })
    .toArray(function (err, reservation) {
      if (err)
        return next(err);
      var r = reservation.map(reser => reser.Visit._id);
      var collection2 = db.collection('visits').find().toArray(function (err, visits) {
        if (err)
          callback(err);
        for (i = 0; i < r.length; i++) {
          for (j = 0; j < visits.length; j++) {
            if (r[i] == visits[j]._id) { //auto konwersja z objectId
              visits[j]._id = -1;
            }
          }
        }
        var result = [];
        for (i = 0; i < visits.length; i++) {
          if (visits[i]._id !== -1)
            result.push(visits[i]);
        }
        res.send(result);
      });
    });
});


app.get("/app/myreservation/:user", function (req, res, next) {
  let _id = req.params.user;
  db.collection('reservation').find({ "User._id": _id }).toArray(function (err, visit) {
    if (err)
      return next(err);
    res.send(visit);
  });
});

app.delete("/app/myreservation/:_id", function (req, res, next) {
  let reservation = req.params._id;
  db.collection('reservation').deleteOne({ "_id": ObjectId(reservation) }, function (err, result) {
    if (err)
      return next(err);
    res.sendStatus(200);
  })

});


app.post("/app/setVisit", function (req, res, next) {
  let reason = req.body.reason;
  let id_doc = req.body.id_doc;
  let user = req.body.user;
  let visit = req.body.visit;
  db.collection('reservation').insert({ "User": user, "Visit": visit, "reason": reason, id_doc: id_doc }, function (err, result) {
    if (err)
      return next(err);
    res.sendStatus(201);
  });
});


app.post("/app/canIlog", function (req, res, next) {
  var id = req.body.id;
  let password = req.body.password;
  db.collection('users').findOne({ /*_*/id: /*ObjectId(*/+id/*)*/ }, { "passwd": password }, function (err, u) {
    if (err)
      return next(err);
    if (u.passwd === password) {
      res.sendStatus(200);
    }
    else res.sendStatus(401);
  });
});


app.get("/app/user/:user", function (req, res, next) {
  var userId = req.params.user;
  db.collection('users').findOne({ id: +userId }, { "name": 1 }, function (err, user) {
    if (err)
      return next(err);
    res.send(user);
  });
});

app.use(function (err, req, res, next) {
  res.sendStatus(500);
});

app.listen(1234, function () {
  console.log("Serwer uruchomiony");
});