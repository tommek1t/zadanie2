import { provideRouter, RouterConfig }  from '@angular/router';
import { DoctorsComponent } from './doctors.component';
import {DashboardComponent} from './dashboard.component';
import {DoctorDetailComponent} from './doctor-detail.component';
import {LoginComponent} from './login.component';
import {VisitDetailComponent} from './visit-detail.component';

const routes: RouterConfig = [
  {
    path: 'doctors',
    component: DoctorsComponent
  },

  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: '',
    redirectTo: '/doctors',
    pathMatch: 'full'
  },
  {
    path: 'detail/:id',
    component: DoctorDetailComponent
  },
  {
    path: 'plan/:id',
    component: DoctorDetailComponent
  },
  {
    path: 'editVisit/:id/:iddoc',
    component: VisitDetailComponent
  },
  {
    path: 'addVisit/:iddoc',
    component: VisitDetailComponent
  },
  {
    path: 'newDoc',
    component: DoctorDetailComponent
  }
  ,
  {
    path: 'users',
    component: LoginComponent
  },
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
