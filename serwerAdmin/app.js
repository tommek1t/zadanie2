var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000");
  res.header("Access-Control-Allow-Headers", ["Origin", "Content-Type"]);
  res.header("Access-Control-Allow-Methods", ["PUT", "DELETE"]);
  next();
});

var urlDB = 'mongodb://localhost:27017/zad2';
MongoClient.connect(urlDB, function (err, database) {
  if (err)
    process.exit(1);

  db = database;
  app.listen(3005);
  console.log("Listening on port 3005 for admin");
});


app.get("/admin/doctors", function (req, res, next) {
  db.collection('doctors').find({}, { _id: 1, name: 1, specjalizacja: 1 }).toArray(function (err, doc) {
    if (err)
      return next(err);
    res.send(doc);
  });
});


app.get("/admin/doctor/:id", function (req, res, next) {
  let iddoc = req.params.id;
  db.collection('doctors').findOne({ _id: ObjectId(iddoc) }, { _id: 1, name: 1, specjalizacja: 1 }, function (err, doc) {
    if (err)
      return next(err);
    res.send(doc);
  });
});


app.delete("/admin/doc/:id", function (req, res, next) {
  let iddoc = req.params.id;
  db.collection('doctors').deleteOne({
    "_id": ObjectId(iddoc)
  }, function (err, results) {
    if (err)
      return next(err);
    res.sendStatus(200);
  }
  );
});

app.delete("/admin/visit/:id", function (req, res, next) {
  let iddoc = req.params.id;
  db.collection('visits').deleteOne({
    "_id": ObjectId(iddoc)
  }, function (err, results) {
    if (err)
      return next(err);
    res.sendStatus(200);
  }
  );

});


app.put("/admin/doc", function (req, res, next) {
  let id = req.body._id;
  let name = req.body.name;
  let specjalizacje = req.body.specjalizacja;
  db.collection('doctors').update({ "_id": ObjectId(id) }, {
    $set: { name: name, specjalizacja: specjalizacje }
  }, function (err, results) {
    if (err)
      return next(err);
    res.sendStatus(200);
  }
  );
});


app.post("/admin/doc", function (req, res, next) {
  let name = req.body.name;
  let specjalizacje = req.body.specjalizacja;
  db.collection('doctors').insert({
    "name": name, "specjalizacja": specjalizacje, "password": "p"
  }, function (err, results) {
    if (err)
      return next(err);
  }
  );
  res.sendStatus(201);
});


app.put("/admin/visit", function (req, res, next) {
  let id = req.body._id;
  let spec = req.body.doctor_specjalizacja;
  let docname = req.body.doctor_name;
  let data = req.body.date;
  let city = req.body.city;
  let hour1 = req.body.hour1;
  let hour2 = req.body.hour2;
  db.collection('visits').update({ "_id": ObjectId(id) }, {
    $set: {
      doctor_specjalizacja: spec, doctor_name: docname, hour1: hour1, hour2: hour2,
      date: data, city: city
    }
  }, function (err, result) {
    if (err)
      return next(err);
    res.sendStatus(200);
  });

});


app.post("/admin/visit", function (req, res, next) {//add new
  let spec = req.body.doctor_specjalizacja;
  let docname = req.body.doctor_name;
  let data = req.body.date;
  let city = req.body.city;
  let iddoc = req.body.id_doc;
  let hour1 = req.body.hour1;
  let hour2 = req.body.hour2;
  db.collection('visits').insert({
    doctor_specjalizacja: spec, doctor_name: docname,
    date: data, city: city, id_doc: ObjectId(iddoc), hour1: hour1, hour2: hour2
  }, function (err, result) {
    if (err)
      return next(err);
    res.sendStatus(201);
  });

});


app.get("/admin/visits/:id", function (req, res, next) {
  let docid = req.params.id;
  db.collection('visits').find({ id_doc: ObjectId(docid) }).toArray(function (err, v) {
    if (err)
      return next(err);
    res.send(v);
  });
});


app.get("/admin/visit/:id", function (req, res, next) {
  let visitid = req.params.id;
  db.collection('visits').findOne({ _id: ObjectId(visitid) }, function (err, v) {
    if (err)
      return next(err);
    res.send(v);
  });
});


app.get("/admin/reservation/:id", function (req, res, next) {
  let docid = req.params.id;
  db.collection('reservation').find({ id_doc: docid }, { User: 1, "Visit._id": 1, reason: 1 }).toArray(function (err, v) {
    if (err)
      return next(err);
    res.send(v);
  });
});


app.get("/admin/reservationByID/:id", function (req, res, next) {
  let id = req.params.id;
  db.collection('reservation').findOne({ _id: ObjectId(id) }, { User: 1, reason: 1 },function (err, v) {
    if (err)
      return next(err);
    res.send(v);
  });
});


app.post("/admin/doctor/canIlog", function (req, res, next) {
  var id = req.body.id;
  let password = req.body.password;
  db.collection('doctors').findOne({ _id: ObjectId(id) }, { "password": password }, function (err, doctor) {
    if (err)
      return next(err);
    if (doctor && doctor.password === password) {
      res.sendStatus(200);
    }
    else res.sendStatus(401);
  });
});


app.use(function (err, req, res, next) {
  res.sendStatus(500);
});


app.listen(1199, function () {
  console.log("SerwerSide for ADMIN working...");
});