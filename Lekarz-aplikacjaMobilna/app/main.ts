// this import should be first in order to load some required settings (like globals and reflect-metadata)
import {nativeScriptBootstrap} from "nativescript-angular/application";
import {AppComponent} from "./app.component";
import {HTTP_PROVIDERS} from "@angular/http";
import {APP_ROUTER_PROVIDERS} from "./app.routes";
import {DoctorService} from './doctor.service'

nativeScriptBootstrap(AppComponent,[DoctorService,HTTP_PROVIDERS,APP_ROUTER_PROVIDERS]);