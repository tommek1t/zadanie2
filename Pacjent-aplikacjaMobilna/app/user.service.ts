import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Visit} from './visit';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {
  user: any;
  private spec: string;//nr spec
  dateFROM: Date;
  dateTO: Date;
  cities: string[];
  specializations: string[] = [];
  visit: Visit;
  serverURL = "http://10.132.226.163:1234/app/";
  visits: any[] = [];

  constructor(private http: Http) { } 

 private getActualVisits() {
    return fetch(this.serverURL + "visits")
      .then(res => Promise.resolve(res))
      .then(res => res.json())
      .then(data => this.visits = data);
  }

  getActualSpecialization() {
    return this.getActualVisits().then(() => {
      let self = this;
      this.visits.forEach(function (el) {
        if (self.specializations.indexOf(el.doctor_specjalizacja) == -1)
          self.specializations.push(el.doctor_specjalizacja);
      });
      return self.specializations;
    });
  }

  getActualCities() {
    let ci: string[] = [];
    return this.getActualVisits().then(() => {
      let self = this;
      this.visits.forEach(function (el) {
        if (self.spec === el.doctor_specjalizacja || self.spec === "all") {
          let data = new Date(el.date);
          if (data >= self.dateFROM && data <= self.dateTO) {
            if (ci.indexOf(el.city) === -1) {
              ci.push(el.city);
            }
          }
        }
      });
      return ci;
    });
  }


  getSpecyficVisits() {
    return this.getActualVisits().then(() => {
      return this.getFilteredVisits();
    });
  }
  getFinalVisit(index: number) {
    let visits = this.getFilteredVisits();
    return visits[index];
  }

  private getFilteredVisits() {
    let result = [];
    let self = this;
    this.visits.forEach(function (el) {
      if (self.spec === el.doctor_specjalizacja || self.spec === "all") {
        if (self.cities.indexOf(el.city) !== -1) {
          let data = new Date(el.date);
          if (data >= self.dateFROM && data <= self.dateTO)
            result.push(el);
        }
      }
    });
    result.sort(function (a, b) {
      if (a.date < b.date) return -1;
      else return 1;
    });
    return result;
  }

  tryLog(id: number, password: string) {
    let body = { "id": id, "password": password };
    return this.http.post(this.serverURL + "canIlog", body);
  }

  setUser(id: number) {
    return this.http.get(this.serverURL + "user/" + id)
      .map(response => response.json())
      .toPromise().then(data => this.user = data);
  }


  setSpec(spec: number) { if (spec === -1) this.spec = "all"; else this.spec = this.specializations[spec]; }
  setOkres(d1: Date, d2: Date) { this.dateFROM = d1; this.dateTO = d2; }
  setCities(cities: string[]) { this.cities = cities; }

  setChooseVisit(v: number) {
    let visitObject = this.getFinalVisit(v)
    this.visit = new Visit();
    this.visit.city = visitObject.city;
    this.visit.date = visitObject.date;
    this.visit.doctor_name = visitObject.doctor_name;
    this.visit.doctor_specjalizacja = visitObject.doctor_specjalizacja;
    this.visit._id = visitObject._id;
    this.visit.id_doc = visitObject.id_doc;
    this.visit.hour1 = visitObject.hour1;
    this.visit.hour2 = visitObject.hour2;
    visitObject = undefined;
  }

  getVisit() { 
    return Promise.resolve(this.visit); }

  getUserName() {
    return Promise.resolve(this.user.name);}

  getMyVisits() {
    return fetch(this.serverURL + "myreservation/" + this.user._id)
      .then(res => res.json())
  }

  pushReservation(visit: Visit, reason: string) {
    let body = { "visit": visit, "reason": reason, "user": this.user, "id_doc": visit.id_doc };
    this.visit = undefined;

    return this.http.post(this.serverURL + "setVisit", body)
  }

  deleteReservation(_id) {
    return this.http.delete(this.serverURL + "myreservation/"+_id).subscribe(
      () => true,
      (err) => false
    );
  }
}