import {Component, OnInit} from "@angular/core";
import {Router} from '@angular/router'
import {UserService} from "./user.service";
import dialogs = require("ui/dialogs");
var applicationSettings = require("application-settings");

@Component({
    selector: "my-reservation",
    templateUrl: "html/myreservation.component.html",
    styleUrls: ["css/forms.css"]
})
export class MyReservationComponent implements OnInit {
    lista: string[] = [];
    obiektyWizyt: any[] = [];
    visitDetail: any;
    selected: boolean = false;
    poz: number;
    reason = "nie podano";
    data: Date;


    public listViewItemTap(pozycja) {
        this.poz = pozycja.index;
        this.visitDetail = this.obiektyWizyt[this.poz].Visit;
        this.data = new Date(this.obiektyWizyt[this.poz].Visit.date);
        this.reason = this.obiektyWizyt[this.poz].reason;
        this.selected = true;
    }

    constructor(private router: Router, private userService: UserService) {
    }

    ifDone() {
        return this.data > new Date();
    }

    delete() {
        dialogs.confirm({
            title: "Ostrzeżenie",
            message: "Usuwajac tę wizytę zwalniasz na nia miejsce!",
            okButtonText: "Tak, wiem. Usuń",
            cancelButtonText: "Anuluj",
        }).then(result => {
            if (result) {
                this.userService.deleteReservation(this.obiektyWizyt[this.poz]._id.valueOf())
                this.lista.splice(this.poz, 1);
                this.obiektyWizyt.splice(this.poz, 1);
                this.selected = false;
            }
        });
    }

    ngOnInit() {
        this.userService.getMyVisits().then(wizyty => {
            wizyty.forEach(el => {
                this.obiektyWizyt.push(el);
                this.lista.push(el.Visit.date + ", " + el.Visit.hour1 + "->" + el.Visit.hour2 + " ("
                    + el.Visit.city + ", " + el.Visit.doctor_specjalizacja + ")")
            });
        });
    }

    logout() {
        this.userService.user = undefined;
        applicationSettings.setString("id_user", "none");
        this.router.navigate(['/']);
    }

    addNew() {
        this.router.navigate(['/spec-okres']);
    }
}
