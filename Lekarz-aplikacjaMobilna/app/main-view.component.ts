import {Component, OnInit} from "@angular/core";
import {Router} from '@angular/router';
import {DoctorService} from './doctor.service';
var applicationSettings = require("application-settings");
var dialogs = require("ui/dialogs");

@Component({
    selector: "main",
    templateUrl: "main-view.component.html",
    styleUrls: ["app.css"]
})

export class MainViewComponent implements OnInit {
    list: any[] = [];
    docreservation: any[] = [];
    longPress: boolean = false;
    selectedVisit: any;
    indexselectedVisit: number;

    constructor(private router: Router, private doctorService: DoctorService) { }

    ngOnInit() {
        if (applicationSettings.getString("id") === undefined)
            this.router.navigate(['/login']);
        else {
            this.doctorService.getMyVisits()
                .then(obiekty => {
                    obiekty.forEach(el => {
                        this.list.push(el);
                    });
                });
            this.doctorService.getReservation(applicationSettings.getString("id"))
                .then(r => this.docreservation = r)
        }
        //registerElement("Fab", () => require("nativescript-floatingactionbutton").Fab);
    }

    addNew() {
        this.router.navigate(['/visit-detail']);
    }



    private isReservated() {
        let ok = true;
        for (let i = 0; i < this.docreservation.length; i++) {
            if (this.docreservation[i].Visit._id === this.selectedVisit)
                ok = false;
        }
        return ok;

    }

    public listViewItemTap(position) {
        this.selectedVisit = this.list[position.index]._id;
        this.indexselectedVisit = position.index;
        let reservation;
        for (let i = 0; i < this.docreservation.length; i++) {
            console.log("in for", i, this.docreservation[i].Visit._id, "selectedVisit:", this.selectedVisit)
            if (this.docreservation[i].Visit._id === this.selectedVisit)
                reservation = this.docreservation[i];
        }
        if (this.longPress === false) {
            if (reservation !== undefined)
                this.router.navigate(['/visit-readOnly', this.list[position.index]._id, reservation._id]);
            else
                this.router.navigate(['/visit-readOnly', this.list[position.index]._id]);
        }
        else
            this.listViewItemLongPress(position);
    }

    public listViewItemLongPress(pozycja) {
        this.longPress = true;
        if (this.selectedVisit) {
            this.dialogWindow();
        }
    }

    dialogWindow() {
        dialogs.confirm({
            title: this.selectedVisit,
            message: "Edytować czy Usunać?",
            okButtonText: "Edytuj",
            cancelButtonText: "Usuń",
        }).then(result => {
            if (this.isReservated()) {
                if (result) {
                    this.router.navigate(['/visit-detail', this.selectedVisit]);
                }
                else {
                    this.deleteWindow();
                }
            }
            else alert("Operacja niedozwolona dla zarezerwowanej wizyty");
            this.longPress = false;
        });
    }

    deleteWindow() {
        dialogs.confirm({
            message: "Usunać pozycje?",
            okButtonText: "Usuń",
            cancelButtonText: "Anuluj",
        }
        ).then(result => {
            if (result)
                this.doctorService.deleteVisit(this.selectedVisit).then(() => {
                    this.list.splice(this.indexselectedVisit, 1);
                    this.indexselectedVisit = undefined;
                    this.selectedVisit = false;
                });

        });
    };

    logOut() {
        applicationSettings.clear();
        this.router.navigate(['/login']);
    }
}