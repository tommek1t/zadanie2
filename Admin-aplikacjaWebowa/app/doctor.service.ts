import {Injectable} from '@angular/core';
import {Doctor} from './doctor';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class DoctorService {

    private doctorURL = 'http://10.132.226.163:1199/admin/';
    constructor(private http: Http) { }

    getDoctors(): Promise<Doctor[]> {
        return this.http.get(this.doctorURL + "doctors")
            .map(response => response.json())
            .toPromise();
    }

    getDoctor(id: number) {
        return this.getDoctors().then(doctors => doctors.find(doc => doc._id == id));
    }

    save(doc: Doctor): Promise<Doctor> {
        if (doc._id) {
            return this.putDoc(doc);
        }
        return this.postDoc(doc).then();
    }

    saveVisit(visit: any): Promise<any> {
        if (visit._id) {
            return this.putVis(visit);
        }
        return this.postVis(visit).then();
    }


    delete(doc: Doctor) {
        return this.http
            .delete(this.doctorURL + "doc/" + doc._id)
            .toPromise()
            .catch(this.handleError);
    }

    getPlan(doc: number) {
        return this.http.get(this.doctorURL + "visits/" + doc).map(response => response.json()).toPromise();
    }

    getReservation(doc: number) {
        return this.http.get(this.doctorURL + "reservation/" + doc).map(response => response.json()).toPromise();
    }

    getVisit(visit: number) {
        return this.http.get(this.doctorURL + "visit/" + visit).map(response => response.json()).toPromise();
    }


    private handleError(error: any) {
        console.error('Blad z DOCTORService', error);
        return Promise.reject(error.message || error);
    }

    private postDoc(doc: Doctor) {//add
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        return this.http
            .post(this.doctorURL + "doc", JSON.stringify(doc), { headers: headers }).toPromise();
    }

    private putDoc(doc: Doctor) {//update
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http
            .put(this.doctorURL + "doc", JSON.stringify(doc), { headers: headers })
            .toPromise()
            .then(() => doc)
            .catch(this.handleError);
    }

    private postVis(vis: any) {//add
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        return this.http
            .post(this.doctorURL + "visit", JSON.stringify(vis), { headers: headers }).toPromise();
    }

    private putVis(vis: any) {//update
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http
            .put(this.doctorURL + "visit", JSON.stringify(vis), { headers: headers })
            .toPromise()
            .then(() => vis)
            .catch(this.handleError);
    }

    deleteVisit(idvisit: any) {
        return this.http
            .delete(this.doctorURL + "visit/" + idvisit)
            .toPromise()
            .catch(this.handleError);
    }


}